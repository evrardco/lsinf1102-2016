package student.examples;


import telegram.TelegramClient;
import telegram.TelegramMessageListener;

import java.io.IOException;


/**
 * @author Maxime Piraux, maxime.piraux@student.uclouvain.be
 * @author Pierre Schaus, pierre.schaus@uclouvain.be
 */
public class StudentDemoTelegramAnsweringQuestions {

    public static void main(String[] args) throws IOException {
        // Création du client Telegram. Vous trouverez plus d'informations sur l'obtention de ces valeurs
        // dans la documentation prévue à cet effet.
    	

        final TelegramClient client = new TelegramClient("xxx bot username xxx", "xxx bot token xxx", "xxx user_id xxx");

    	
        // Ajout d'un listener en instanciant une classe anonyme implémentant TelegramMessageListener.
        // Lorsque vous accéder des variables déclarés à l'extérieur d'une classe anonyme, celles-ci doivent être final.
        // Un TelegramMessageListener verra sa méthode messageReceived appelée lorsque l'utilisateur
        // envoie un message au bot sur Telegram.
        client.addListener(new TelegramMessageListener() {
            @Override
            public void messageReceived(String text) {
            	// si le message contient un "?" ...
                if (text.contains("?")) {
                    try {
                    	// ... alors on répond avec une réponse simple ;-)
                        client.sendMessage("I have the answer !", "42");
                        
                        // notez que client.pushFile permet d'envoyer des fichiers
                        // par exemple client.pushFile("dayGraph.png", null);
                    } catch (IOException ex) {
                        // Erreur d'I/O lors de l'envoi du message
                        // Affichage de l'erreur et arret du programme.
                        ex.printStackTrace();
                        System.exit(-1);
                    }
                }
            }
        });

    }
}