package student.linearregression;
public class LinearRegression {


    /**
     * @param x the x coordinates
     * @param y the y coordinates i.e. point i is defined as (x[i],y[i])
     * @param alpha the learning rate of the gradient descent
     *      (a small positive value, typically 0.001)
     * @param iter  the number of iteration of the gradient descent algorithm
     * @return a line equation that fits at best (least-square error) the given points using the "gradient descent algorihtm" initialized with m=0 (slope), b=0 (intercept).
     */
    public static StraightLine fitLine(double [] x, double [] y, double alpha, int iter) {
        // TODO 
        return new StraightLine(0,0);
    }
}